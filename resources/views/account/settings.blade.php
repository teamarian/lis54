@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    @if(Auth::user()->avatar !== "avatar.jpg")
                    <img alt="" class="img-responsive" src="/uploads/<?php echo Auth::user()->avatar;?>" />
                    @else
                    <img src="/uploads/avatars/avatar.jpg" class="img-responsive" alt="">
                    @endif
                     </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> {{ $user->name }} {{ $user->lname }} </div>
                    <div class="profile-usertitle-job"> {{ $user->course }} </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                    <!-- <button type="button" class="btn btn-circle green btn-sm">Follow</button> -->
                    <!--<button type="button" class="btn btn-circle red btn-sm">Message</button>-->
                </div>
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <!-- <li>
                            <a href="#">
                                <i class="icon-home"></i> Overview </a>
                        </li> -->
                        <li class="active">
                            <a href="#">
                                <i class="icon-settings"></i> Account Settings </a>
                        </li>
                        <!--<li>
                            <a href="page_user_profile_1_help.html">
                                <i class="icon-info"></i> Help </a>
                        </li>-->
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light ">
                <!-- STAT -->
                <!--<div class="row list-separated profile-stat">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="uppercase profile-stat-title"> 37 </div>
                        <div class="uppercase profile-stat-text"> Projects </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="uppercase profile-stat-title"> 51 </div>
                        <div class="uppercase profile-stat-text"> Tasks </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="uppercase profile-stat-title"> 61 </div>
                        <div class="uppercase profile-stat-text"> Uploads </div>
                    </div>
                </div>-->
                <!-- END STAT -->
                <div>
                    <h4 class="profile-desc-title">About {{ $user->name }} {{ $user->lname }}</h4>
                    <span class="profile-desc-text"> {{ $user->about }} </span>
                    @if($user->website_url)
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-globe"></i>
                        <a href="{{$user->website_url}}" target="_blank">{{ $user->website_url }}</a>
                    </div>
                    @endif
                    @if($user->twitter_url)
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-twitter"></i>
                        <a href="{{$user->twitter_url}}" target="_blank">@twitter</a>
                    </div>
                    @endif
                    @if($user->facebook_url)
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-facebook"></i>
                        <a href="{{$user->facebook_url}}" target="_blank">facebook</a>
                    </div>
                    @endif
                </div>
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Account Settings</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#personal-info-tab" data-toggle="tab" aria-expanded="true">Personal Info</a>
                                </li>
                                <li class="">
                                    <a href="#change-avatar-tab" data-toggle="tab" aria-expanded="false">Change Avatar</a>
                                </li>
                                <li class="">
                                    <a href="#change-password-tab" data-toggle="tab" aria-expanded="false">Change Password</a>
                                </li>
                                <!--<li class="">
                                    <a href="#tab_1_4" data-toggle="tab" aria-expanded="false">Privacy Settings</a>
                                </li>-->
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="personal-info-tab">
                                    <form role="form" action="#">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label class="control-label">First Name</label>
                                            <input value="{{ $user->name }}" name="name" class="form-control" type="text"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Last Name</label>
                                            <input value="{{ $user->lname }}" name="lname" class="form-control" type="text"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Mobile Number</label>
                                            <input value="{{ $user->phone }}" name="phone" class="form-control" type="text"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Interests</label>
                                            <input value="{{ $user->interest }}" name="interest" class="form-control" type="text"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Course</label>
                                            <input value="{{ $user->course }}" name="course" class="form-control" type="text"> </div>
                                        <div class="form-group">
                                            <label class="control-label">About</label>
                                            <textarea class="form-control" rows="3" name="about" value="{{ $user->about }}"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Website Url</label>
                                            <input value="{{ $user->website_url }}" name="website_url" class="form-control" type="text"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Facebook Url</label>
                                            <input value="{{ $user->facebook_url }}" name="facebook_url" class="form-control" type="text"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Twitter Url</label>
                                            <input value="{{ $user->twitter_url }}" name="twitter_url" class="form-control" type="text"> </div>
                                        <div class="margiv-top-10">
                                            <!-- <a href="javascript:;" class="btn green submit"> Save Changes </a> -->
                                            <a href="#" id="form-submit" class="btn green submit ladda-button" data-style="expand-right" data-size="l"><span class="ladda-label">Submit form</span></a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane" id="change-avatar-tab">
                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                        eiusmod. </p>
                                    <form action="#" role="form" id="form-change-avatar" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input name="avatar-image" type="file" required> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10">
                                                <span class="label label-danger">NOTE! </span>
                                                <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <!-- <a href="javascript:;" class="btn green submit"> Submit </a> -->
                                            <a href="javascript:;" class="btn green submit ladda-button" data-style="expand-right" data-size="l"><span class="ladda-label">Submit</span></a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE AVATAR TAB -->
                                <!-- CHANGE PASSWORD TAB -->
                                <div class="tab-pane" id="change-password-tab">
                                    <form action id="change-password" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="form-type" value="change_password">
                                        <div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input class="form-control" name="current_password" id="current_password" type="password"> </div>
                                        <div class="form-group">
                                            <label class="control-label">New Password</label>
                                            <input class="form-control" name="new_password" id="new_password" type="password"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Re-type New Password</label>
                                            <input class="form-control" name="confirm_new_password" id="confirm_new_password" type="password"> </div>
                                        <div class="margin-top-10">
                                            <!-- <a href="javascript:;" class="btn green change-password"> Change Password </a>
                                             -->
                                            <!-- <button type="submit" class="btn green change-password"> Change Password </button> -->
                                            <a href="javascript:;" class="btn green submit ladda-button" data-style="expand-right" data-size="l"><span class="ladda-label">Submit</span></a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE PASSWORD TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>

@endsection

@section('plugin-script')
    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <script src="/js/account.js" type="text/javascript"></script>

@endsection

@section('page-css')
    <link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
    <style>
        label.error { color: red; }
    </style>
@endsection
