@extends('layouts.admin')

@section('content')

<div class="form-group form-md-line-input form-md-floating-label" style="padding-top: 0px;">
    <input type="hidden" class="_token" name="_token" value="{{ csrf_token() }}">
    <input type="text" class="form-control" id="input-search">
    <label for="input-search"></label>
    <span class="help-block">e.g Hemoglobin</span>
</div>

<div class="row" id="thumbs">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('archive') }}">
            <div class="visual">
                <i class="fa fa-book"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span>Archives</span>
                    <div class="desc">Go to your archive</div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="{{ route('settings') }}">
            <div class="visual">
                <i class="fa fa-gear"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span></span> Settings </div>
                    <div class="desc">Need to change something?</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-question"></i>
            </div>
            
            <div class="details">
                <div class="number">
                    <span>Activities</div>
                <div class="desc">View your recent searches</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="{{ route('support') }}">
            <div class="visual">
                <i class="fa fa-binoculars"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span>Support</span>
                </div>
                <div class="desc">Need help?</div>
            </div>
        </a>
    </div>
</div>

<div class="row search-result-box">
  <div class="col-lg-12 col-xs-12 col-sm-12">
      <div class="portlet light bordered">
          <div class="portlet-title tabbable-line">
              <div class="caption">
                  <i class="icon-bubbles font-dark hide"></i>
                  <span class="caption-subject font-dark bold uppercase result-number"></span>
                  <span class="caption-subject font-dark bold uppercase">results for </span>
                  <span class="caption-subject font-dark bold uppercase">
                    "<span class="query"></span>"
                  </span>
              </div>
          </div>
          <div class="portlet-body">
              <div class="tab-content">
                  <div class="tab-pane active" id="content-result">

                  </div>
              </div>

            

          </div>          
        </div>
  </div>
</div>
<div>
    <center><img class="load-spinner" src="/assets/global/img/input-spinner.gif" alt="loading..." /></center>
</div>
<!-- start modal -->
<div class="modal fade" id="basic" tabindex="-1" data-id="" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Abstract</h4>
                <em class="title"></em>
            </div>
            <div class="modal-body">
            <p></p>
            <p class="abstract">Not available, click "Full Text" instead.</p> </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn green pull-left btn-modal-add-to-archive">Add to Archive</button> -->
                <a href="javascript:;" class="btn green pull-left btn-modal-add-to-archive ladda-button" data-style="expand-right" data-size="l"><span class="ladda-label">Add to Archives</span></a>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="button" class="btn green btn-modal-full-text full-text">Full Text</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('plugin-script')
<script src="/assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
<script src="./js/search/search.js"></script>
@endsection

@section('page-css')
<link href="/assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
<style>
    .mt-comment-details .mt-comment-actions { 
        display: inline-block !important;
        font-size: 12px;
    }
    a.result-item-modal,
    a.result-item-modal:hover,
    a.result-item-modal:active {
        text-decoration: inherit;
        color: #333;
    }
    .abstract-true {
        color: #31c5d1;
    }
    .mt-comments {
        cursor: pointer;
    }
</style>
@endsection