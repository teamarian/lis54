@extends('layouts.admin')

@section('content')




<div class="col-md-6">

<img style="max-width: 100%;" src="http://djnoman.com/wp-content/uploads/2016/05/intelliverse-contact-center-banners.jpg" alt="" />
<p>Need assistance with our <strong>{{ config('app.name') }}</strong> Platform? We'll get you the help you need.</p>
<!-- <img style="max-width: 100%;" src="http://www.anser.com/wp-content/uploads/2015/09/bkd-csr.jpg" alt=""> -->
</div>

<div class="portlet light bordered col-md-6">

    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <i class="fa fa-paper-plane font-red-sunglo"></i>
            <span class="caption-subject bold uppercase">Send Us A Message</span>
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" id="email-support">
            <div class="form-body">
                <div class="form-group">
                    <label>Is this your email address?</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <input type="text" name="name" class="form-control" value="{{ Auth::user()->email }}" required> </div>
                </div>
                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" name="subject" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>How can we help you?</label>
                    <textarea name="message" class="form-control" rows="3" required></textarea>
                </div>
            </div>                
            <div class="form-actions">
                <button type="submit" class="btn blue btn-send">Send Message</button>
                <!-- <button type="button" class="btn default">Cancel</button> -->
            </div>
        </form>
    </div>
</div>


@endsection


@section('page-css')
<link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style>
    input.error, textarea.error { border: 1px solid red; }
    label.error { display: none !important; }
</style>
@endsection

@section('plugin-script')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
@endsection

@section('page-script')
<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="/js/support.js" type="text/javascript"></script>
@endsection