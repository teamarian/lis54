@component('mail::message')
# {{ $heading }}

{{ $message }}

Thanks,<br>
{{ $user->name }}
@endcomponent
