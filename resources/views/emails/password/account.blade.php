@component('mail::message')
# Introduction

Welcome!

Your new password is: {{ $password }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
