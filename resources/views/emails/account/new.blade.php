@component('mail::message')
# Welcome

Hi {{$user->name}}!

Thank you for creating an account with us. You're all ready to go!

{{ config('app.name') }} makes it easier for you to get updated information you want.
It brings more flexibility and insight to power your need through a 
multiple features that our pltform provides. It lets you keep up with the updated 
article and titles provided by {{ config('app.name') }}.

For more information about Open Access, including download and login links, please got to {{ config('app.url') }}.

Thanks!<br/>
The {{ config('app.name' )}} Team

@endcomponent