@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Manage Users</span>
                </div>
                
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="{{ route('newUser') }}" class="btn sbold green"> Add User
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="table-users" role="grid" aria-describedby="sample_1_info">
                    <thead>
                        <tr role="row">
                        <!-- <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 104px;" aria-label="">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input class="group-checkable" data-set="#sample_1 .checkboxes" type="checkbox">
                                    <span></span>
                                </label>
                            </th> -->
                            <th class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 256px;" aria-sort="ascending" aria-label=" Username : activate to sort column descending"> Username </th>
                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 375px;" aria-label=" Email : activate to sort column ascending"> Email </th>
                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 200px;" aria-label=" Status : activate to sort column ascending"> Status </th>
                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 199px;" aria-label=" Joined : activate to sort column ascending"> Joined </th>
                            <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" style="width: 125px;" aria-label=" Actions : activate to sort column ascending"> Actions </th>
                            </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

@endsection

@section('page-css')
<link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style>
    .label-admin { background-color: #36c6d3 !important; }
    .label-banned { background-color: #e7505a !important; }
    .sorting_1 { padding-left: 10px !important; text-align: inherit !important; }
</style>
@endsection

@section('plugin-script')
<script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
@endsection

@section('page-script')
<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="/js/users.js" type="text/javascript"></script>
@endsection