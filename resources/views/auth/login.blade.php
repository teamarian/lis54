@extends('layouts.app2')

@section('content')

<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 bs-reset mt-login-5-bsfix">
            <div class="login-bg" style="background-image:url(../assets/pages/img/login/bg1.jpg)">
                <!-- <img class="login-logo" src="../assets/pages/img/login/logo.png" />  --></div>
        </div>
        <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
            <div class="login-content">
                <h1>Login</h1>
                <p> Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing. </p>
                
                <form method="POST" action class="login-form"> 
                <!-- <form method="POST" action class="login-form"> -->
                    {{ csrf_field() }}
                    @if ($errors->has('email'))
                        <div class="alert alert-danger display-hide" style="display: block;">
                            <button class="close" data-close="alert"></button>
                            <span> {{ $errors->first('email') }} </span>
                        </div>
                    @endif
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        <span class="alert-danger-message">Enter any username and password</span>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Enter your email" name="email" required/> </div>
                        <div class="col-xs-6">
                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="password" required/> </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="rem-password">
                                <label class="rememberme mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" name="remember" value="1" /> Remember me
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-8 text-right">
                            <div class="forgot-password">
                                <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a> | 
                                <a href="{{ url('register') }}" id="create-account" class="forget-password">Create an account</a>
                            </div>
                            <button class="btn green" type="submit">Sign In</button>
                        </div>
                    </div>
                </form>
                <!-- BEGIN FORGOT PASSWORD FORM -->
                <form class="forget-form" action="javascript:;" method="post">
                    <h3 class="font-green">Forgot Password ?</h3>
                    <p> Enter your e-mail address below to reset your password. </p>
                    <div class="form-group">
                        <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                    <div class="form-actions">
                        <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                        <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                    </div>
                </form>
                <!-- END FORGOT PASSWORD FORM -->
            </div>
            <div class="login-footer">
                <div class="row bs-reset">
                    <div class="col-xs-5 bs-reset">
                        <ul class="login-social">
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-twitter"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-7 bs-reset">
                        <div class="login-copyright text-right"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END : LOGIN PAGE 5-1 -->

@endsection

@section('plugins')
<script src="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
@endsection

@section('scripts')
<script src="/assets/pages/scripts/login-5.min.js" type="text/javascript"></script> 
<script>
    // jQuery(document).ready(function($) {
    //     $('.login-form button[type="submit"]').on('click', function(e) {
    //         if($('.login-form').valid() === true) {
    //             e.preventDefault();                

    //             $form_login = $('.login-form').serializeArray();
    //             $form_login.data = { 'email': null, 'password': null };
    //             $form_login.data.email = $form_login[1].value;
    //             $form_login.data.password = $form_login[2].value;

    //             $.ajax({
    //                 url: "api/authenticate",
    //                 dataType: "json",
    //                 contentType: "application/json;charset=utf-8",
    //                 type: "POST",
    //                 statusCode: {
    //                     401: function(data) {
    //                         $('.login-form .alert.alert-danger .alert-danger-message').text('Invalid login credentials. Please, try again.');
    //                         $('.login-form .alert.alert-danger').show('fast');
    //                     }
    //                 },
    //                 data: JSON.stringify($form_login.data),
    //                 success: function (data) {
    //                    if(data.status === 200) {
    //                        console.log(data);
    //                        localStorage.setItem('_token',data._token);
    //                        setTimeout(function() {
    //                             $('.login-form').submit();
    //                        }, 1000);
                           
    //                    }
    //                 }
    //             });

    //         }
    //     });
    // });
</script>
@endsection