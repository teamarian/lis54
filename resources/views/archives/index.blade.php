@extends('layouts.admin')

@section('content')

<div class="row search-result-box">
  <div class="col-lg-12 col-xs-12 col-sm-12">
      <div class="portlet light bordered">
          <div class="portlet-title tabbable-line">
              <!-- <div class="caption">
                  <i class="icon-bubbles font-dark hide"></i>
                  <span class="caption-subject font-dark bold uppercase result-number"></span>
                  <span class="caption-subject font-dark bold uppercase">results for </span>
                  <span class="caption-subject font-dark bold uppercase">
                  "<span class="query"></span>"
                  </span>
              </div> -->
          </div>
          <div class="portlet-body">
              <div class="tab-content">
                  <div class="tab-pane active" id="content-result">
                  	<h1>No record available</h1>
                  </div>
              </div>
          </div>          
        </div>
  </div>
</div>

<!-- Modal -->
<!-- start modal -->
<div class="modal fade" id="basic" tabindex="-1" data-id="" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Abstract</h4>
                <em class="title"></em>
            </div>
            <div class="modal-body">
            <p></p>
            <p class="abstract">Not available, click "Full Text" instead.</p> </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn green pull-left btn-modal-add-to-archive">Add to Archive</button> -->
                <!-- <a href="javascript:;" class="btn red pull-left btn-modal-add-to-archive ladda-button btn-delete-archive" data-style="expand-right" data-size="l" ><span class="ladda-label">Delete</span></a> -->
                <button class="btn btn-danger btn-del-archive mt-sweetalert pull-left">Delete Archive</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="button" class="btn green btn-modal-full-text full-text">Full Text</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('page-css')
<link href="/assets/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<style>
    .mt-comment-details .mt-comment-actions { 
        display: inline-block !important;
        font-size: 12px;
    }
    a.result-item-modal,
    a.result-item-modal:hover,
    a.result-item-modal:active {
        text-decoration: inherit;
        color: #333;
    }
    .abstract-true {
        color: #31c5d1;
    }
    .search-result-box { display: none; }
    #basic .title {
        color: #31c5d1;
        font-size: 11px;
        font-style: inherit;
    }
    .mt-comments { cursor: pointer; }
</style>
@endsection

@section('plugin-script')
<script src="/assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="./js/archive.js"></script>
@endsection