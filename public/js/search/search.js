jQuery(document).ready(function($) {
                
var nextPage = "";
var queryResults = null;
var currentFullText = null;
var retrieving = false;

$('.search-result-box').hide();
$('.load-spinner').hide();

$('#input-search').on('keyup', function(e) {                    
    retrieveSearchQuery(this,e);
});

$("#basic").on("shown.bs.modal", function (e) { 
    var _index = $(e.relatedTarget).data('id');
    $('.abstract').text('Not available, click "Full Text" instead.');
    populateModal(_index);
    $(this).attr('data-id',_index);
});

$('.btn-modal-full-text').on('click', function(e) {
    window.open(currentFullText,'_blank');
});

$('.btn-modal-add-to-archive').on('click', function(e) {
    
    e.stopPropagation();
    e.preventDefault();
    
    var l = Ladda.create(this);  
    l.start();
    
    setTimeout(function() {
        
        var _data_id = $('#basic').attr('data-id');
        var v = queryResults[_data_id];
        
        var title = v.bibjson.title;
        var source = v.bibjson.journal.title;
        var abstract = v.bibjson.abstract;
        var year = v.bibjson.year;
        var volume = v.bibjson.journal.volume;
        var issue = v.bibjson.journal.number;
        var authors = retrieveAuthors(v.bibjson.author);
        var fullText = v.bibjson.link[0].url

        var _formData = {
            'journalTitle': source,
            'articleTitle': title,
            'year': year,
            'issue': issue,
            'volume': volume,
            'abstract': v.bibjson.abstract,
            'authors': authors,
            'fullText': fullText,
            '_token': window.Laravel.csrfToken
        };

        $.post('/archives',
            _formData,
            function(res) {
                if(203 === res) {
                    l.stop();
                    return true;
                }
                $("#basic").modal('toggle');
                l.stop();
                return true;
            });

    }, 1000);

});

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
        if(typeof nextPage !== 'undefined') {
            if(retrieving === false) {
                retrieving = true;

                retrieveNextPage(function() {
                    retrieving = false;
                });
                
            }               
        }
    }
    return false;
});

function retrieveAuthors(arrayNames) {
    var names = "";
    $.each(arrayNames, function(i,v) {
            names += v.name + ", ";                        
    });
    names = names.slice(0,-2);
    return names;
}

function retrieveNextPage(callback) {

    App.startPageLoading({ animate: true });
    $.get(nextPage, function(data) {
        nextPage = data.next;
        pushToQueryResults(data.results, function() {
            appendResultItems(data.results, true);
        });                        

        App.stopPageLoading();
        callback();
        return false;
    });

}

function pushToQueryResults(array, callback) {
    $.each(array, function(i,v) {
        queryResults.push(v);                    
    });

    callback();
}

function populateModal(id) {
    var data = queryResults[id];
    $('.abstract').html(data.bibjson.abstract);
    if(typeof data.bibjson.link[0].url !== "undefined") {
        currentFullText = data.bibjson.link[0].url;
    }
}

function retrieveSearchQuery(that,e) {

    if(e.keyCode == 13) {

        var  ivalue = $(that).val();
        var  _query = ivalue;
        var _token = $('._token').val();

        if(ivalue === "") return;

        $('#content-result').html('');
        $('.search-result-box').hide();

        App.startPageLoading({ animate: true });

        $.get('/search/fetch',{ article: ivalue },function(data, status) {

            var data = JSON.parse(data);
            var results = data.results;

            queryResults = results;

            nextPage = data.next;
            appendResultItems(results);

            App.stopPageLoading();
            $('.help-block').hide();
            $('.result-number').text(data.total);
            $('.query').text(_query);

        });
    }
}                

function appendResultItems(results, append) {                    

    if(queryResults == null)
        return;

    var countIndex = queryResults.length;

    $.each(results, function(i,v) {

        if(append === true)
            i = countIndex + i - 10;                        

        var title = v.bibjson.title;
        var source = v.bibjson.journal.title;
        var abstract = v.bibjson.abstract;
        var year = v.bibjson.year;
        var volume = v.bibjson.journal.volume;
        var issue = v.bibjson.journal.number;
        var authors = retrieveAuthors(v.bibjson.author);

        var template = `
        
        <div class="mt-comments" data-id="[index]" data-toggle="modal" data-target="#basic">
            <div class="mt-comment">
                <div class="mt-comment-img">
                    <i class="fa fa-file fa-2x [abstract-true]"></i> 
                </div>
                <div class="mt-comment-body">
                <div class="mt-comment-info">
                    <span class="mt-comment-author"> [title] </span>                    
                </div>
                <div class="mt-comment-text"> [authors] </div>
                <div class="mt-comment-details">
                    <ul class="mt-comment-actions pull-left">
                        <li style="color: #31c5d1; padding-left: inherit;">[source].</li>
                        <li>
                            [year]
                            [volume]
                            [issue]
                        </li>
                    </ul>
                </div>
            </div>
        </div>`;

        template = template.replace('[title]',title);
        template = template.replace('[authors]',authors);
        template = template.replace('[source]',source);
        template = template.replace('[index]',i);

        if(typeof abstract !== "undefined" 
            && abstract !== "" 
            && abstract !== "."
            && abstract !== "Oral presentation is available online"
            && abstract !== "Not required") {
                template = template.replace('[abstract-true]','abstract-true');
            }

        if(typeof year !== "undefined")
            template = template.replace('[year]',year + "; ");
        else 
            template = template.replace('[year]',"");
        if(typeof volume !== "undefined")
            template = template.replace('[volume]',volume);
        else
            template = template.replace('[volume]',"");
        if(typeof issue !== "undefined")
            template = template.replace('[issue]',"(" + issue + ")");
        else
            template = template.replace('[issue]',"");

        $('#content-result').append(template);
        $('#thumbs').hide('slow');
        $('.search-result-box').show();
        
    });

}

});