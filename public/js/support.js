'use strict';

jQuery(document).ready(function($) {

    $("#email-support").validate({
        submitHandler: function(form) {

            App.startPageLoading({ animate: true });

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': window.Laravel.csrfToken
                    },
                    url: '/support/send',
                    data: $('#email-support').serializeArray(),
                    dataType: 'jsonp',
                    method: 'POST',
                    complete: function(res) {
                        App.stopPageLoading();
                        if(res.responseJSON.status == 201) {
                            swal("Oops!", "Unable to send message", "warning");
                            return;
                        }
                        $('#new-user')[0].reset();
                        swal("Message Sent!", "Our Support Team will get back to you soon!", "success");
                        return;
                    }
                });

        }
    });

});