'use strict';

jQuery(document).ready(function($) {

	window.Archives;
	var retrieving = false;

	App.startPageLoading({ animate: true });

	init();

	function init() {
		retrieving = true;
		$.ajax({
			url: '/archive',
			dataType: 'jsonp',
			method: 'GET',
			complete: function(res) {
				var data = JSON.parse(res.responseText);
				window.Archives = {data: data[0].archives.data};
				window.Archives.next_page_url = data[0].archives.next_page_url;
				retrieving = false;
				$(".search-result-box").show();
				
				tmpItem(window.Archives.data, function() {
					App.stopPageLoading();
				});		
			}
		});
	}


	function tmpItem(data,callback,append = false) {

		var countIndex = window.Archives.data.length;			

		$.each(data, function(i,v) {

			if(append === true)
				i = countIndex + i - 10;
			
			var abstract = v.abstract;

			var tmp = `<div class="mt-comments" data-id="[index]" data-toggle="modal" data-target="#basic">
							<div class="mt-comment">
								<div class="mt-comment-img">
									<i class="fa fa-file fa-2x [abstract-true]"></i> </div>
								<div class="mt-comment-body">
								<div class="mt-comment-info">
									<span class="mt-comment-author"> [title] </span>
								</div>
								<div class="mt-comment-text"> [authors] </div>
								<div class="mt-comment-details">
									<ul class="mt-comment-actions pull-left">
										<li style="color: #31c5d1; padding-left: inherit;">[source].</li>
										<li>
											[year]
											[volume]
											[issue]
										</li>
									</ul>
								</div>
							</div>
						</div>`;	

			tmp = tmp.replace('[index]',v.id);
			tmp = tmp.replace('[title]',v.articleTitle);
			tmp = tmp.replace('[authors]',v.authors);
			tmp = tmp.replace('[source]',v.journalTitle);
			tmp = tmp.replace('[year]',v.year);
			tmp = tmp.replace('[volume]',v.volume);
			tmp = tmp.replace('[issue]',v.issue);

			if(typeof abstract !== "undefined" 
            && abstract !== "" 
            && abstract !== "."
            && abstract !== "Oral presentation is available online"
            && abstract !== "Not required") {
                tmp = tmp.replace('[abstract-true]','abstract-true');
            }


			$('#content-result h1').hide();
			$('#content-result').append(tmp);

		});

		callback();
		
	}

	function populateModal(id) {

		$.each(window.Archives.data, function(i,v) {
			if(id == v.id) {
				$('.abstract').html(v.abstract);
				$('#basic .title').html(v.journalTitle);
				if(typeof v.fullText !== "undefined") {
					window.Archives.currentFullText = v.fullText
				}
			}
		});

	}

	$("#basic").on("shown.bs.modal", function (e) { 
		var _index = $(e.relatedTarget).data('id');
		console.log(_index);
		$('.abstract').text('Not available, click "Full Text" instead.');
		populateModal(_index);
		$(this).attr('data-id',_index);
	});

	$('.btn-modal-full-text').on('click', function(e) {
		window.open(window.Archives.currentFullText,'_blank');
	});

	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() == $(document).height()) {
			if(window.Archives.next_page_url != null) {
				if(retrieving === false) {
					retrieving = true;
					App.startPageLoading({ animate: true });
					retrieveNextPage(function() {
						App.stopPageLoading();
						retrieving = false;
						return;
					});
					
				}               
			}
		}
		return false;
	});

	function retrieveNextPage(callback) {

		$.ajax({
			url: window.Archives.next_page_url,
			dataType: 'jsonp',
			method: 'GET',
			complete: function(res) {
				var data = JSON.parse(res.responseText);
				window.Archives.next_page_url = data[0].archives.next_page_url;
				pushToQueryResults(data[0].archives.data, function() {
					tmpItem(data[0].archives.data, function() {
						App.stopPageLoading();
					},true);
				});	
			}
		});
	}

	function pushToQueryResults(array, callback) {
		$.each(array, function(i,v) {
			window.Archives.data.push(v);                    
		});
		callback();
	}

	$('#basic .btn-del-archive').on('click', function(e) {
		var modal = $('#basic').modal('toggle');
		var id = $('#basic').attr('data-id');
		swal({
			title: "Are you sure?",
			text: "Your will not be able to recover this archive!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		  },
		  function(confirm){
			if(confirm) {
				$.each(window.Archives.data, function(i,v) {
					if(id == v.id) {
						
						$.ajax({
							headers: {
								'X-CSRF-TOKEN': window.Laravel.csrfToken
							},
							url: '/archive/' + id,
							dataType: 'json',
							method: 'DELETE',
							complete: function(res) {
								console.log(res.responseJSON);
								var select = 'div[data-id="'+ id +'"]';
								$(select).hide('slow');
								swal("Deleted!", "Your archive file has been deleted.", "success");
							}
						});
					}								
				});				
			}
		  });
	});

});