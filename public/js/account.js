'use strict';

jQuery(document).ready(function($) {
    $('#change-password').validate({
        rules: {
            current_password: {
                required: true,
                minlength: 6
            },
            new_password: {
                required: true,
                minlength: 8,
            },
            confirm_new_password: {
                required: true,
                equalTo: '#new_password'
            }
        },
        messages: {
            current_password: {
                required: 'Enter your current passowrd',
            },
            new_password: {
                required: 'Enter your new password',
            },
            confirm_new_password: {
                required: 'Enter your new password again',
                equalTo: 'Enter your new password again'
            }
        }
    });

    $('form#change-password a.submit').on('click', function(e) {
        
        e.stopPropagation();
        e.preventDefault();
        
        var l = Ladda.create(this);            
        var data = new FormData($('#change-password')[0]);
        l.start();

        setTimeout(function() {
            $.ajax({
                url:'/account/update-password',
                data: data,
                dataType:'json',
                async:false,
                type:'post',
                processData: false,
                contentType: false,
                success:function(response){
                    if(response.status !== 200) {
                        swal("Oops!", "Invalid current password", "error");
                        return false;
                    } else {
                        swal("Success!", "Your account password has been updated", "success");
                        $('#change-password')[0].reset();
                    }
                    return true;
                },
                complete: function() {
                    l.stop();
                }
            });
        }, 1000);


        // if($('#change-password').valid() == true) {
        //     var _pw_data = $('#change-password').serializeArray();
        //     $.post('/api/account/update-password?token=' + localStorage.getItem('_token'), 
        //         _pw_data,function(data) {
        //         if(data.status === 200) {
                   
        //         } else {
        //             toastr.error("Invalid current password", "System Notification");
        //         }
        //     });
        // }
    });

    // Avatar Form
    var img_file;

    $('#form-change-avatar input[type="file"]').on('change', function(e) {
        img_file = e.target.files;
    });

    $('#form-change-avatar a.submit').on('click', function(e) {

        e.stopPropagation();
        e.preventDefault();
        
        var l = Ladda.create(this);                
        var data = new FormData($('#form-change-avatar')[0]);
        l.start();

        setTimeout(function() {
            $.ajax({
                url:'/account/update-photo',
                data: data,
                dataType:'json',
                async:false,
                type:'post',
                processData: false,
                contentType: false,
                complete: function(res) {

                    l.stop();

                    if(res.responseJSON == 201) {
                        swal("Oops!", "Missing avatar file", "error");
                        l.stop();
                        return;
                    } else {
                        $('.img-circle').attr('src','/uploads/' + res.responseJSON.file);
                        $('.profile-userpic img.img-responsive').attr('src','/uploads/' + res.responseJSON.file);
                        $('#form-change-avatar input[type="file"]').attr('value','');
                        swal("Success!", "Your avatar has been updated.", "success");
                        return;
                    }
                    
                }
            });
        }, 1000);

    });

    $('#personal-info-tab a.submit').on('click', function(e) {

        e.stopPropagation();
        e.preventDefault();

        var l = Ladda.create(this);                
        var data = new FormData($('#personal-info-tab form')[0]);
        l.start();

        setTimeout(function() {
            $.ajax({
                url:'/account/update-account',
                data: data,
                dataType:'json',
                async:false,
                type:'post',
                processData: false,
                contentType: false,
                statusCode: {
                    200: function() {
                        swal("Success!", "Your account has been updated.", "success");
                    }
                },
                complete: function() {
                    l.stop();
                }
            });
        }, 1000);
        

    });

});