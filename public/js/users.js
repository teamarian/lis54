'use strict';

jQuery(document).ready(function($) {

    window.App.users = new Array();

    App.startPageLoading({ animate: true });

    $.ajax({
        url: '/users',
        dataType: 'jsonp',
        method: 'GET',
        complete: function(res) {
            let data = JSON.parse(res.responseText);
            window.App.users = data[0].users;
            appendItems(window.App.users, function() {               

                    App.stopPageLoading();
                    deleteUser();
                    setUserRole();
                    $('#table-users').DataTable();
                
            });            
        }
    });

    var appendItems = function(items,callback) {
        $.each(items, function(i,v) {
            $('#table-users tbody').append(_templateItem(v));
        });
        callback();
    }

    var _templateItem = function(item) {

        

        return `
            <tr>
                <td><img alt="" class="img-circle avatar" src="uploads/`+item.avatar+`"> `+item.name+`</td>
                <td>`+item.email+`</td>
                <td><span class="label label-primary label-`+changeBanText(item.roles[0].name)+`">`+changeBanText(item.roles[0].name)+`</span></td>
                <td>`+item.created_at+`</td>
                <td>
                    <div class="btn-group">
                        <button class="btn blue dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Action
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="/user/` + item.id + `/edit"> Edit </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="btn-delete-user" data-id="`+item.id+`"> Delete </a>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-submenu">
                                <a href="javascript:;">Set role as</a>
                                <ul class="dropdown-menu" style="left: -105%; top: 0px;">
                                <li>
                                    <a href="javascript:;" class="btn-set-role" data-id="`+item.id+`" data-role="admin">Admin</a>
                                </li>                               
                                <li>
                                    <a href="javascript:;" class="btn-set-role" data-id="`+item.id+`" data-role="regular">Regular</a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="btn-set-role" data-id="`+item.id+`" data-role="ban">Ban</a>
                                </li>
                            </ul>
                            </li>
                        </ul>
                        </li>
                        </ul>
                    </div>
                </td>
            </tr>
        `;

    }

    var deleteUser = function(id) {
        $('.btn-delete-user').on('click', function(e) {
            
            var id = $(this).data('id');
            var _this = this;

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
              },
              function(confirm){
                if(confirm) {                   
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': window.Laravel.csrfToken
                        },
                        url: '/user/' + id,
                        dataType: 'json',
                        method: 'DELETE',
                        complete: function(res) {
                            if(res.responseJSON.status == 201) {
                                swal("Oops!", "Unable to delete admin user", "warning");
                                return;
                            }
                            swal("Deleted!", "User has been deleted", "success");
                            $(_this).closest('tr').hide('slow');
                            return;
                        }
                    });
                }
              });

        });
    }

    var setUserRole = function(id) {

        $('.btn-set-role').on('click', function(e) {
            
            var id = $(this).data('id');
            var role = $(this).data('role');
            var _this = this;

            swal({
                title: "Hey!",
                text: "Do you want to set this user as "+ role +"?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, please!",
                closeOnConfirm: false
              },
              function(confirm){
                if(confirm) {                   
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': window.Laravel.csrfToken
                        },
                        url: '/user/role/' + id + '/' + role,
                        dataType: 'json',
                        method: 'GET',
                        complete: function(res) {
                            if(res.responseJSON.status == 201) {
                                swal("Oops!", "Unable to set user as " + role, "warning");
                                return;
                            }
                            swal("Hey!", "User role has been set to " + role, "success");
                            changeStatusRow(role,_this);
                            return;
                        }
                    });
                }
              });

        });
    }

    var changeStatusRow = function(status,_this) {
        
        var label_role = $(_this).closest('tr');
        label_role = $(label_role).find('span.label-primary');

        if(status === "admin") {
            label_role.text("admin");
            label_role.removeClass('label-regular');
            label_role.removeClass('label-banned');
            label_role.addClass('label-admin');
        }

        if(status === "regular") {
            label_role.text("regular");
            label_role.removeClass('label-banned');
            label_role.removeClass('label-admin');
            label_role.addClass('label-regular');
        }

        if(status === "ban") {
            label_role.text("banned");
            label_role.removeClass('label-regular');
            label_role.removeClass('label-admin');
            label_role.addClass('label-banned');
        }

    }

    var changeBanText = function(text) {
        if(text == "ban") {
            return "banned";
        }
        return text;
    }

    $('.btn-create-user').on('click', function() {    
        App.startPageLoading({ animate: true });  
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': window.Laravel.csrfToken
            },
            url: '/users',
            data: $('#new-user').serializeArray(),
            dataType: 'json',
            method: 'POST',
            complete: function(res) {
                App.stopPageLoading();
                if(res.responseJSON.status == 201) {
                    swal("Oops!", "Unable to create new user", "warning");
                    return;
                }
                $('#new-user')[0].reset();
                swal("Created!", "A random string password has been sent to user's email", "success");
                return;

            }
        });

    });



});