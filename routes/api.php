<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('jwt.auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('jwt.auth')->get('/hash', 'ToolsController@index');
Route::middleware('jwt.auth')->post('/account/update-password', 'AccountController@update_password');
Route::middleware('jwt.auth')->get('/search', 'SearchController@search');
Route::middleware('jwt.auth')->post('/account/update-photo', 'AccountController@UploadPhoto');
Route::middleware('jwt.auth')->post('/account/update-account', 'AccountController@UpdateAccount');
Route::middleware('jwt.auth')->post('/archives', 'ArchivesController@create');
Route::middleware('jwt.auth')->get('/archives', 'ArchivesController@apiIndex');
Route::get('/archives', 'ArchivesController@apiIndex');

Route::post('/authenticate', 'UserController@authenticate');