<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function() {
    if(Auth::user()) {
      return redirect('search');
    } else {
      return redirect('login');
    }
});

Route::get('/', function() {
  if(Auth::user()) {
    return redirect('search');
  } else {
    return redirect('login');
  }
});

Auth::routes();

Route::middleware('auth')->get('/search', 'SearchController@index')->name('search');
Route::middleware('auth')->get('/search/fetch', 'SearchController@search');
Route::middleware('auth')->get('/archive', 'ArchivesController@index')->name('archive');
Route::get('/logout', function() {
  Auth::logout();
  return redirect('login');
})->name('logout');

Route::middleware('auth')->get('/account/settings', 'AccountController@settings')->name('settings');
Route::middleware('auth')->post('/account/settings', 'AccountController@settings')->name('settings');
Route::middleware('auth')->post('/archives', 'ArchivesController@create');
Route::middleware('auth')->delete('/archive/{id}', 'ArchivesController@delete')->where('id', '[0-9]+');
Route::middleware('auth')->post('/account/update-account', 'AccountController@UpdateAccount');
Route::middleware('auth')->post('/account/update-photo', 'AccountController@UploadPhoto');
Route::middleware('auth')->post('/account/update-password', 'AccountController@update_password');


// Users
Route::middleware('auth')->get('/users', 'UserController@index')->name('users');
Route::middleware('auth')->post('/users', 'UserController@create')->name('users');
Route::middleware('auth')->get('/users/new', 'UserController@create')->name('newUser');
Route::middleware('auth')->delete('/user/{id}', 'UserController@delete')->where('id', '[0-9]+');
Route::middleware('auth')->get('/user/role/{id}/{role}', 'UserController@setRole');
Route::middleware('auth')->get('/user/{id}/edit', 'UserController@edit');
Route::middleware('auth')->post('/user/{id}/edit', 'UserController@UpdateAccount');
Route::middleware('auth')->post('/user/{id}/avatar', 'UserController@UploadPhoto');


Route::middleware('auth')->get('/support', 'SupportController@index')->name('support');
Route::middleware('auth')->post('/support/send', 'SupportController@send')->name('support-send');