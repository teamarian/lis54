<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_regular = Role::where('name', 'regular')->first();
        $role_admin  = Role::where('name', 'admin')->first();
        
        $manager = new User();
        $manager->name = 'Caesar Ian';
        $manager->email = 'moreishi@gmail.com';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($role_admin);

        $manager = new User();
        $manager->name = 'Arlene';
        $manager->email = 'apmonotilla@swu.edu.ph';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($role_admin);

        $manager = new User();
        $manager->name = 'Caesar Ian';
        $manager->email = 'moreishi@ymail.com';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($role_regular);

        $manager = new User();
        $manager->name = 'Admin Demo';
        $manager->email = 'admin@admin.com';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($role_admin);

        for ($i = 0; $i <= 30; $i++) {
            $employee = new User();
            $employee->name = 'Employee Name ' . $i;
            $employee->email = 'employee'.$i.'@example.com';
            $employee->password = bcrypt('secret');
            $employee->save();
            $employee->roles()->attach($role_regular);
        }
        
    }
}
