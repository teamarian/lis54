<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'regular';
        $role_employee->description = 'A regular user';
        $role_employee->save();
    
        $role_manager = new Role();
        $role_manager->name = 'admin';
        $role_manager->description = 'An admin user';
        $role_manager->save();

        $role_manager = new Role();
        $role_manager->name = 'moderator';
        $role_manager->description = 'A moderator user';
        $role_manager->save();

        $role_manager = new Role();
        $role_manager->name = 'ban';
        $role_manager->description = 'A banned user';
        $role_manager->save();

    }
}
