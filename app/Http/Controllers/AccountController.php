<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class AccountController extends Controller
{

    public function update_password(Request $request) {        
        
        if(!$request->ajax()) {
            return response()->json(['status' => 201, 'message' => 'Unable to change password'], 200);
        }

        $userId = Auth::user()->id;
        $user = User::find($userId);

        if (Hash::check($request->input('current_password'), $user->password)) {
            $user->password = Hash::make($request->input('new_password'));
            $user->save();
        } else {
            return response()->json(['status' => 201, 'message' => 'Invalid Password'], 200);
        }

        return response()->json(['status' => 200, 'message' => 'success', compact('password')], 200);

    }

    public function settings(Request $request) {
        $user = Auth::user();
        return view('account.settings', ['title' => 'Account', 'user' => $user,'navStatus' => null]);
    }

    public function UploadPhoto(Request $request) {

        if(!$request->ajax()) {
            return response()->json(['message' => 'failed' ,'stauts' => 201], 200);
        }

        if (!$request->hasFile('avatar-image')) {
            return $request->json(['status' => 201, 'message' => 'Invalid file'], 201);
        }

        if (!$request->file('avatar-image')->isValid()) {
            return $request->json(['status' => 201, 'message' => 'Invalid file'], 201);
        }

        // $url = Storage::putFile('avatars', $request->file('avatar-image'));
        $file = $request->file('avatar-image');
        $fileName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        $newFileName = time().'_'.$fileName;
        $filePath = public_path('uploads/avatars/'.$newFileName);
        $fileUrl = 'avatars/'.$newFileName;

        $img = Image::make($request->file('avatar-image'));
        $img->resize(320, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($filePath);

        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->avatar = $fileUrl;
        $user->update();

        return response()->json(['message' => 'success' ,'file' => $fileUrl],200);
    }

    public function UpdateAccount(Request $request) {

        if($request->ajax()) {
            if($request->isMethod('POST')) {
                $user = User::find(Auth::user()->id);
                $user->update($request->all());
            }
            return response()->json(['message' => 'success']);
        }

        return response()->json(['message' => 'failed','status' => 201]);
        
    }

}