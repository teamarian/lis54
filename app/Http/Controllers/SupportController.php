<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NewSupport;
use Illuminate\Support\Facades\Mail;

use App\User;

class SupportController extends Controller
{
    public function index() {
        return view('support.index',['title' => 'Need Help?', 'navStatus' => 'navSupport']);
    }

    public function send(Request $request) {

        $user = $request->user();

        if($request->ajax()) {
            Mail::to($user)
                  ->queue(new NewSupport($user,$request->get('message'),$request->get('subject')));
            return response()->json(['status' => 200,'message' => 'Message sent!']);
        }

    }

}
