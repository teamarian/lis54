<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archive as Archive;
use App\User as User;
Use Auth;

class ArchivesController extends Controller
{
    public function __construct() {}

    public function index(Request $request) {

        // Check request if ajax
        if($request->ajax()) {
            $user = Auth::user();
            $archives = User::find($user->id)->archives()->paginate(10);
            return response()->json([compact('archives')],200);
        }
        
        return view('archives.index', ['title' => 'Archives','navStatus' => 'navArchive']);
    }

    public function create(Request $request) {

        $user = Auth::User();

        $archive = new Archive();
        $archive->journalTitle = $request->get('journalTitle');
        $archive->articleTitle = $request->get('articleTitle');
        $archive->year = $request->get('year');

        $archive->issue = ($request->get('issue')) ? $request->get('issue') : "";

        $archive->volume = $request->get('volume');

        $abstract = "";

        if($request->get('abstract') != "") {
            $abstract = $request->get('abstract');
        }


        $checkTitle = User::find($user->id)->archives()->where('articleTitle','=',$archive->articleTitle)->count();

        if($checkTitle > 0) {
            return response()->json(['success' => false],200);
        }

        $archive->abstract = $abstract;
        $archive->authors = $request->get('authors');
        $archive->fullText = $request->get('fullText');
        $archive->user_id = $user->id;
        $archive->save();

        return response()->json(['success' => true],200);
        
    }

    public function delete($id = null, Request $request) {
        $archive = User::find(Auth::user()->id)->archives()->where('id','=',$id)->count();
        if($archive > 0) {
            Archive::where('id',$id)->delete();
        }
        return response()->json(['success' => true, compact('archive')],200);
    }

}
