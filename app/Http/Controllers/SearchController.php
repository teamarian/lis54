<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class SearchController extends Controller
{

    public function index(Request $request) {
      return view('search.index', ['title' => 'Search','navStatus' => 'navSearch']);
    }

    public function search(Request $request) {
      
      $article = $request->input('article');
      $nextPage = $request->input('nextpage');

      if($nextPage) {
        $url = $nextPage;
      } else {
        $url = "https://doaj.org/api/v1/search/articles/".$article."?sort=bibjson.year:desc";
      }     

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
      $output = curl_exec($ch);

      return response()->json($output);

    }

}
