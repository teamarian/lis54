<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use Auth;
use App\User;
use App\Role;

use App\Mail\NewAccountPassword;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    public function __construct() {

    }

    public function index(Request $request) {
        
        if(!$request->user()->hasRole(['admin'])) {
            return redirect()->route('login');
        }

        if($request->ajax()) {

            if(!$request->user()->authorizeRoles('admin')) {
                return response()->json(array(
                    'status' => 201,
                    'message' => 'Unauthorized Access'
                ));
            }
            $users = User::with(['roles'])->get();
            return response()->json([compact('users')]);
        }

        return view('users.index', array('title' => 'Users', 'navStatus' => 'navUsers'));
    }

    public function authenticate(Request $request) {

        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required'
        ]);
        
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(['status' => 200,'message' => 'success','_token' => $token], 200);

    }

    public function delete($id = null, Request $request) {
        
        if($request->ajax()) {            
            if(!$request->user()->authorizeRoles('admin')) {
                return response()->json(array(
                    'status' => 201,
                    'message' => 'Unauthorized Access'
                ));
            }
            if(!$id || $id === null) {
                return response()->json(array(
                    'status' => 201,
                    'message' => 'No item to delete'
                ));
            }

            $user = User::find($id);
            $isAdmin = $user->hasRole('admin');
            if($isAdmin) {
                return response()->json(array(
                    'status' => 201,
                    'message' => 'Unable to delete admin'
                ));
            }

            $user->delete();

            return response()->json(['status' => 200, 'message' => 'User removed']);
        }

    }

    public function setRole($id = null, $role = "regular",Request $request) {

        $role_regular = Role::where('name', 'regular')->first();
        $role_admin  = Role::where('name', 'admin')->first();
        $role_ban  = Role::where('name', 'ban')->first();

        if($request->ajax()) {
            
            $user = User::find($id);

            if($role === "admin") {
                $user->roles()->detach($role_regular);
                $user->roles()->attach($role_admin);
                $user->roles()->detach($role_ban);
            }
            if($role === "regular") {
                $user->roles()->attach($role_regular);
                $user->roles()->detach($role_admin);
                $user->roles()->detach($role_ban);
            }
            if($role === "ban") {
                $user->roles()->attach($role_ban);
                $user->roles()->detach($role_admin);
                $user->roles()->detach($role_regular);
            }

            return response()->json(['status' => 200, 'message' => 'User role has been change to '.$role]);
        }

    }

    public function create(Request $request) {

        if($request->ajax()) {
            
            $random_password = str_random(12);

            $user = User::create([
                'name'     => $request->get('name'),
                'email'    => $request->get('email'),
                'password' => bcrypt($random_password),
                ]);
            $user
                ->roles()
                ->attach(Role::where('name', 'regular')->first());
            
            Mail::to($user->email)->queue(new NewAccountPassword($random_password));

            return response()->json(['status' => 200, 'message' => 'success']);
        }

        return view('users.create',['navStatus' => 'navUsers', 'title' => 'Add New User']);
    }

    public function edit($id)
    {
        return view('users.edit',['navStatus' => 'navUsers'])->withUser(User::find($id))
                ->withTitle('Edit User');
    }

    public function UpdateAccount($id, Request $request) {
        
        if($request->ajax()) {
            if($request->isMethod('POST')) {
                $user = User::find($id);
                $user->update($request->all());
            }
            return response()->json(['message' => 'success']);
        }

        return response()->json(['message' => 'failed','status' => 201]);
        
    }

    public function UploadPhoto($id,Request $request) {
        
                if(!$request->ajax()) {
                    return response()->json(['message' => 'failed' ,'stauts' => 201], 200);
                }
        
                if (!$request->hasFile('avatar-image')) {
                    return $request->json(['status' => 201, 'message' => 'Invalid file'], 201);
                }
        
                if (!$request->file('avatar-image')->isValid()) {
                    return $request->json(['status' => 201, 'message' => 'Invalid file'], 201);
                }
        
                // $url = Storage::putFile('avatars', $request->file('avatar-image'));
                $file = $request->file('avatar-image');
                $fileName = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                $newFileName = time().'_'.$fileName;
                $filePath = public_path('uploads/avatars/'.$newFileName);
                $fileUrl = 'avatars/'.$newFileName;
        
                $img = Image::make($request->file('avatar-image'));
                $img->resize(320, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($filePath);
        
                $userId = $id;
                $user = User::find($userId);
                $user->avatar = $fileUrl;
                $user->update();
        
                return response()->json(['message' => 'success' ,'file' => $fileUrl],200);
            }

}
