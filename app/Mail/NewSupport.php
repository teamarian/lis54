<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class NewSupport extends Mailable
{
    use Queueable, SerializesModels;

    public $message;
    public $heading;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$message,$subject)
    {
        $this->user = $user;
        $this->message = $message;
        $this->heading = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $random = "Open Access Support: #" . str_random(10);
        return $this->from($this->user->email)
                    ->subject($random)
                    ->markdown('emails.support.new');
    }
}
